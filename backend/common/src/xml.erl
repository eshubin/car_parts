-module(xml).

-include_lib("eunit/include/eunit.hrl").

iconv_name(<<"windows-1251">>) ->
    "WINDOWS-1251".


init() ->
    {ok, MP} = re:compile("<\\?xml.* encoding=\"(.+)\".*\\?>", [anchored]),
    MP.

get_encoding(MP, Xml) ->
    case re:split(Xml, MP, [trim]) of
        [<<>>, Encoding| Tail] ->
            {ok, iconv_name(Encoding), Tail};
        Res ->
            ?debugVal(Res),
            notfound
    end.





test_encoding(MP) ->
    ?assertEqual(
         notfound,
         get_encoding(MP, "sklajflksj<?xml encoding=\"windows-1251\" ?>dsfasdf")
        ),
    ?assertMatch(
         {ok, "WINDOWS-1251", _},
         get_encoding(MP, "<?xml encoding=\"windows-1251\" ?>dsfasdf")
        ),
    ?assertMatch(
         {ok, "WINDOWS-1251", _},
         get_encoding(MP, "<?xml version=\"1.0\" encoding=\"windows-1251\" ?>")
        ).
fixture_test_() ->
    {
	setup,
        fun init/0,
        {with, [fun test_encoding/1]}
    }.
