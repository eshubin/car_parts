%% @author author <author@example.com>
%% @copyright YYYY author.

%% @doc Callbacks for the rest_api application.

-module(rest_api_app).
-author('author <author@example.com>').

-behaviour(application).
-export([start/2,stop/1]).


%% @spec start(_Type, _StartArgs) -> ServerRet
%% @doc application start callback for rest_api.
start(_Type, _StartArgs) ->
    rest_api_sup:start_link().

%% @spec stop(_State) -> ServerRet
%% @doc application stop callback for rest_api.
stop(_State) ->
    ok.
