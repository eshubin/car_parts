-module(document).

-export([new/0]).
-export([set_value/3, get_value/2, append_value/3, append_value_list/3]).

-include("logic_formula.hrl").

-type document() :: dict().


-spec new() ->
		 document().
new() ->
    dict:new().

-spec get_value(document(), property()) ->
		       value().
get_value(Document, Property) ->
    dict:find(Property, Document).

set_value(Document, Property, Value) ->
    dict:store(Property, Value, Document).

append_value(Document, Property, Value) ->
    dict:append(Property, Value, Document).

append_value_list(Document, Property, Values) when is_list(Values) ->
    dict:append_list(Property, Values, Document).
