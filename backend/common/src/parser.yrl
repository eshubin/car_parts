Nonterminals query str.
Terminals dict_word.
Rootsymbol str.

str -> '$empty' : null.
str -> query : '$1'.

query -> query dict_word : {'and', ['$1', word2filter('$2')]}.
query -> dict_word : word2filter('$1').

Erlang code.
word2filter({dict_word, [Id]}) ->
	{dict_word, Id}; 
word2filter({dict_word, List}) when is_list(List) ->
	{'or', [{dict_word, X} || X <- List]}.
