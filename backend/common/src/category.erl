-module(category).

-export([new/0, delete/1]).
-export([add_category/2]).

-record(structure,
	{
	    graph,
	    dict
	}
       ).


-include_lib("eunit/include/eunit.hrl").


new() ->
    #structure{
       graph = digraph:new([cyclic, protected]),
       dict = dict:new()
      }.

add_category(
    #structure{
	 graph = Graph,
	 dict = Dict
	} = Cat,
    Value
   ) ->
    V = digraph:add_vertex(Graph),
    digraph:add_vertex(Graph, V, Value),
    D = dict:store(Value, V, Dict),
    Cat#structure{
	dict = D
       }.

set_relation(
    #structure{
	 graph = Graph,
	 dict = Dict
	} = Cat,
    Child, Parent
   ) ->
    digraph:add_edge(
	Graph,
	dict:fetch(Parent, Dict),
	dict:fetch(Child, Dict)
       ),
    Cat.

get_children( 
    #structure{
	 graph = Graph,
	 dict = Dict
	},
    Category
   ) ->
    [
     tree:get_data(Graph, C) ||
	C <-
	    digraph_utils:reachable(
		[dict:fetch(Category, Dict)],
		Graph
	       )
    ].

delete(
    #structure{
	 graph = Graph
	}
   ) ->
    digraph:delete(Graph).


test_creation(Category) ->
    C = add_category(Category, 1),
    C2 = add_category(C, 2),
    set_relation(C2, 1, 2),
    ?assertEqual([1,2], get_children(C2, 2)),
    ?assertEqual([1], get_children(C2, 1)).    

fixture_test_() ->
    {foreach,
     local,
     fun new/0,
     fun delete/1,
     [
      {with, [fun test_creation/1]}
     ]
    }.

