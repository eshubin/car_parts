-module(tree).

-export([new/0, delete/1]).
-export([invariant/1]).
-export([create_node/2, delete_node/2, get_data/2]).
-export([set_parent/3, add_children/3, get_parent/2, get_children/2, get_root/1]).

-include_lib("eunit/include/eunit.hrl").

-type tree() :: digraph().
-type tree_node() :: digraph:vertex().
-type data() :: digraph:label().

-spec new() ->
		 tree().
new() ->
    digraph:new([acyclic, protected]).


-spec delete(tree()) ->
		    true.
delete(Tree) ->
    digraph:delete(Tree).

-spec create_node(tree(), data()) ->
			 tree_node().
create_node(Tree, Data) ->
    Vertex = digraph:add_vertex(Tree),
    digraph:add_vertex(Tree, Vertex, Data).

-spec delete_node(tree(), tree_node()) ->
			 true.
delete_node(Tree, Node) ->
    digraph:del_vertex(Tree, Node).

-spec get_data(tree(), tree_node()) ->
			 data().
get_data(Tree, Node) ->
    {Node, Data} = digraph:vertex(Tree, Node),
    Data.


-spec set_parent(tree(), tree_node(), tree_node()) ->
			ok.		
set_parent(Tree, Child, Parent) ->
    case digraph:add_edge(Tree, Parent, Child) of
	{error, Reason} ->
	    error({tree_error, Reason});
	_Edge ->
	    ok
    end.

-spec add_children(tree(), tree_node(), [tree_node()]) ->
			  ok.
add_children(Tree, Parent, Children) ->
    lists:foreach(
	fun(Child) ->
		set_parent(Tree, Child, Parent)
	end,
	Children
       ),
    ok.

-spec get_parent(tree(), tree_node()) ->
			false | {ok, tree_node()}.
get_parent(Tree, Node) ->
    %% TODO: resolve thid issue in logic formula
    %% ?assert(invariant(Tree)),
    case digraph:in_neighbours(Tree, Node)
    of
	[] ->
	    false;
	[Parent] ->
	    {ok, Parent}
    end.

-spec get_children(tree(), tree_node()) ->
			[tree_node()].
get_children(Tree, Node) ->
    %% TODO: resolve thid issue in logic formula
    %% ?assert(invariant(Tree)),
    digraph:out_neighbours(Tree, Node).

-spec invariant(tree()) ->
		       boolean().
invariant(Tree) ->
    (0 =:= digraph:no_vertices(Tree)) orelse digraph_utils:is_arborescence(Tree).
%% TODO: need investigation wich invariant to use
    %% (0 =:= digraph:no_vertices(Tree)) orelse
    %% 	digraph_utils:is_tree(Tree).

-spec is_leaf(tree(), tree_node()) ->
			boolean().
is_leaf(Tree, Node) ->
    ?assert(invariant(Tree)),
    0 =:= digraph:out_degree(Tree, Node).

-spec is_root(tree(), tree_node()) ->
			boolean().
is_root(Tree, Node) ->
    ?assert(invariant(Tree)),
    0 =:= digraph:in_degree(Tree, Node).

-spec get_root(tree()) ->
			tree_node().
get_root(Tree) ->
    ?assert(invariant(Tree)),
    digraph_utils:arborescence_root(Tree).



%% ===================================================================
%% EUnit tests
%% ===================================================================


test_empty_tree_root(Tree) ->
    ?assertEqual(no, get_root(Tree)).

%% Test functions
test_create(Tree) ->
    ?assert(invariant(Tree)),
    Node = create_node(Tree, 34),
    ?assert(invariant(Tree)),
    ?assertEqual(34, get_data(Tree, Node)),
    ?assertError(_, get_data(Tree, fake)),
    delete_node(Tree, Node),
    ?assert(invariant(Tree)),
    ?assertError(_, get_data(Tree, Node)),
    ?assert(delete_node(Tree, Node)).

test_relation(Tree) ->
    ?assert(invariant(Tree)),
    Child = create_node(Tree, child0),
    ?assert(invariant(Tree)),
    Parent = create_node(Tree, parent),
    ?assertNot(invariant(Tree)),
    set_parent(Tree, Child, Parent),
    ?assert(invariant(Tree)),
    ?assertError(
       {tree_error, _},
       set_parent(Tree, fake, Parent)
      ),
    add_children(
      Tree,
      Parent,
      [
       create_node(Tree, child1),
       create_node(Tree, child2)
      ]
     ),
    ?assert(invariant(Tree)),
    ?assertMatch({ok, Parent}, get_parent(Tree, Child)),
    ?assertNot(get_parent(Tree, Parent)),
    ?assertEqual(0, length(get_children(Tree, Child))),
    ?assertEqual(3, length(get_children(Tree, Parent))),

    ?assert(is_root(Tree, Parent)),
    ?assert(is_leaf(Tree, Child)),

    ?assertEqual({yes, Parent}, get_root(Tree)).

fixture_test_() ->
    {foreach,
     local,
     fun new/0,
     fun delete/1,
     [
      {with, [fun test_create/1]},
      {with, [fun test_relation/1]},
      {with, [fun test_empty_tree_root/1]}
     ]
    }.
