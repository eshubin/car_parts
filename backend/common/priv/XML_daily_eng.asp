<?xml version="1.0" encoding="utf-8" ?>
<ValCurs Date="03.03.2012" name="Foreign Currency Market">
<Valute ID="R01010">
	<NumCode>036</NumCode>
	<CharCode>AUD</CharCode>
	<Nominal>1</Nominal>
	<Name>Australian Dollar</Name>
	<Value>31,5694</Value>
</Valute>
<Valute ID="R01020A">
	<NumCode>944</NumCode>
	<CharCode>AZN</CharCode>
	<Nominal>1</Nominal>
	<Name>Azerbaijan Manat</Name>
	<Value>37,3150</Value>
</Valute>
<Valute ID="R01035">
	<NumCode>826</NumCode>
	<CharCode>GBP</CharCode>
	<Nominal>1</Nominal>
	<Name>British Pound Sterling</Name>
	<Value>46,6480</Value>
</Valute>
<Valute ID="R01060">
	<NumCode>051</NumCode>
	<CharCode>AMD</CharCode>
	<Nominal>1000</Nominal>
	<Name>Armenia Dram</Name>
	<Value>75,3111</Value>
</Valute>
<Valute ID="R01090">
	<NumCode>974</NumCode>
	<CharCode>BYR</CharCode>
	<Nominal>10000</Nominal>
	<Name>Belarussian Ruble</Name>
	<Value>36,0788</Value>
</Valute>
<Valute ID="R01100">
	<NumCode>975</NumCode>
	<CharCode>BGN</CharCode>
	<Nominal>1</Nominal>
	<Name>Bulgarian lev</Name>
	<Value>19,8914</Value>
</Valute>
<Valute ID="R01115">
	<NumCode>986</NumCode>
	<CharCode>BRL</CharCode>
	<Nominal>1</Nominal>
	<Name>Brazil Real</Name>
	<Value>17,0942</Value>
</Valute>
<Valute ID="R01135">
	<NumCode>348</NumCode>
	<CharCode>HUF</CharCode>
	<Nominal>100</Nominal>
	<Name>Hungarian Forint</Name>
	<Value>13,5223</Value>
</Valute>
<Valute ID="R01215">
	<NumCode>208</NumCode>
	<CharCode>DKK</CharCode>
	<Nominal>10</Nominal>
	<Name>Danish Krone</Name>
	<Value>52,3236</Value>
</Valute>
<Valute ID="R01235">
	<NumCode>840</NumCode>
	<CharCode>USD</CharCode>
	<Nominal>1</Nominal>
	<Name>US Dollar</Name>
	<Value>29,2960</Value>
</Valute>
<Valute ID="R01239">
	<NumCode>978</NumCode>
	<CharCode>EUR</CharCode>
	<Nominal>1</Nominal>
	<Name>Euro</Name>
	<Value>38,9490</Value>
</Valute>
<Valute ID="R01270">
	<NumCode>356</NumCode>
	<CharCode>INR</CharCode>
	<Nominal>100</Nominal>
	<Name>Indian Rupee</Name>
	<Value>59,2497</Value>
</Valute>
<Valute ID="R01335">
	<NumCode>398</NumCode>
	<CharCode>KZT</CharCode>
	<Nominal>100</Nominal>
	<Name>Kazakh Tenge</Name>
	<Value>19,8060</Value>
</Valute>
<Valute ID="R01350">
	<NumCode>124</NumCode>
	<CharCode>CAD</CharCode>
	<Nominal>1</Nominal>
	<Name>Canadian Dollar</Name>
	<Value>29,6759</Value>
</Valute>
<Valute ID="R01370">
	<NumCode>417</NumCode>
	<CharCode>KGS</CharCode>
	<Nominal>100</Nominal>
	<Name>Kyrgyzstan Som</Name>
	<Value>62,6898</Value>
</Valute>
<Valute ID="R01375">
	<NumCode>156</NumCode>
	<CharCode>CNY</CharCode>
	<Nominal>10</Nominal>
	<Name>China Yuan</Name>
	<Value>46,5193</Value>
</Valute>
<Valute ID="R01405">
	<NumCode>428</NumCode>
	<CharCode>LVL</CharCode>
	<Nominal>1</Nominal>
	<Name>Latvian Lat</Name>
	<Value>55,6958</Value>
</Valute>
<Valute ID="R01435">
	<NumCode>440</NumCode>
	<CharCode>LTL</CharCode>
	<Nominal>1</Nominal>
	<Name>Lithuanian Lita</Name>
	<Value>11,2655</Value>
</Valute>
<Valute ID="R01500">
	<NumCode>498</NumCode>
	<CharCode>MDL</CharCode>
	<Nominal>10</Nominal>
	<Name>Moldova Lei</Name>
	<Value>24,7876</Value>
</Valute>
<Valute ID="R01535">
	<NumCode>578</NumCode>
	<CharCode>NOK</CharCode>
	<Nominal>10</Nominal>
	<Name>Norwegian Krone</Name>
	<Value>52,4088</Value>
</Valute>
<Valute ID="R01565">
	<NumCode>985</NumCode>
	<CharCode>PLN</CharCode>
	<Nominal>10</Nominal>
	<Name>Polish Zloty</Name>
	<Value>94,7018</Value>
</Valute>
<Valute ID="R01585F">
	<NumCode>946</NumCode>
	<CharCode>RON</CharCode>
	<Nominal>10</Nominal>
	<Name>Romanian Leu</Name>
	<Value>89,4070</Value>
</Valute>
<Valute ID="R01589">
	<NumCode>960</NumCode>
	<CharCode>XDR</CharCode>
	<Nominal>1</Nominal>
	<Name>SDR</Name>
	<Value>45,4129</Value>
</Valute>
<Valute ID="R01625">
	<NumCode>702</NumCode>
	<CharCode>SGD</CharCode>
	<Nominal>1</Nominal>
	<Name>Singapore Dollar</Name>
	<Value>23,4481</Value>
</Valute>
<Valute ID="R01670">
	<NumCode>972</NumCode>
	<CharCode>TJS</CharCode>
	<Nominal>10</Nominal>
	<Name>Tajikistan Ruble</Name>
	<Value>61,5501</Value>
</Valute>
<Valute ID="R01700J">
	<NumCode>949</NumCode>
	<CharCode>TRY</CharCode>
	<Nominal>1</Nominal>
	<Name>Turkish Lira</Name>
	<Value>16,6407</Value>
</Valute>
<Valute ID="R01710A">
	<NumCode>934</NumCode>
	<CharCode>TMT</CharCode>
	<Nominal>1</Nominal>
	<Name>New Turkmenistan Manat</Name>
	<Value>10,2721</Value>
</Valute>
<Valute ID="R01717">
	<NumCode>860</NumCode>
	<CharCode>UZS</CharCode>
	<Nominal>1000</Nominal>
	<Name>Uzbekistan Sum</Name>
	<Value>16,0272</Value>
</Valute>
<Valute ID="R01720">
	<NumCode>980</NumCode>
	<CharCode>UAH</CharCode>
	<Nominal>10</Nominal>
	<Name>Ukrainian Hryvnia</Name>
	<Value>36,4809</Value>
</Valute>
<Valute ID="R01760">
	<NumCode>203</NumCode>
	<CharCode>CZK</CharCode>
	<Nominal>10</Nominal>
	<Name>Czech Koruna</Name>
	<Value>15,7100</Value>
</Valute>
<Valute ID="R01770">
	<NumCode>752</NumCode>
	<CharCode>SEK</CharCode>
	<Nominal>10</Nominal>
	<Name>Swedish Krona</Name>
	<Value>44,0999</Value>
</Valute>
<Valute ID="R01775">
	<NumCode>756</NumCode>
	<CharCode>CHF</CharCode>
	<Nominal>1</Nominal>
	<Name>Swiss Franc</Name>
	<Value>32,2395</Value>
</Valute>
<Valute ID="R01810">
	<NumCode>710</NumCode>
	<CharCode>ZAR</CharCode>
	<Nominal>10</Nominal>
	<Name>S.African Rand</Name>
	<Value>39,1360</Value>
</Valute>
<Valute ID="R01815">
	<NumCode>410</NumCode>
	<CharCode>KRW</CharCode>
	<Nominal>1000</Nominal>
	<Name>South Korean Won</Name>
	<Value>26,2603</Value>
</Valute>
<Valute ID="R01820">
	<NumCode>392</NumCode>
	<CharCode>JPY</CharCode>
	<Nominal>100</Nominal>
	<Name>Japanese Yen</Name>
	<Value>35,9042</Value>
</Valute>
</ValCurs>
