-module(currency).

-include_lib("xmerl/include/xmerl.hrl").

-include_lib("eunit/include/eunit.hrl").


get_rate(Xml, Currency) ->
    [#xmlElement{content = Cont}] = xmerl_xpath:string("/ValCurs/Valute[CharCode='" ++ Currency ++ "']/Value", Xml),
    [#xmlText{value = Rate}] = Cont,
    list_to_float(Rate).




test_rate(Xml) ->
    ?assertEqual(29.2960, get_rate(Xml, "USD")),
    ?assertEqual(38.9490, get_rate(Xml, "EUR")).    

test_xml(Xml) ->
    [#xmlElement{content = Cont}] = xmerl_xpath:string("/ValCurs/Valute[CharCode='USD']/Value", Xml),
    [#xmlText{value = Rate}] = Cont,
    ?assertEqual(29.2960, list_to_float(Rate)).

fixture_test_() ->
    {
	foreach,
	fun() ->
		{Xml, []} = xmerl_scan:file("../priv/XML_daily_eng.asp"),
		Xml
	end,
	[
	 {with, [fun test_xml/1]},
	 {with, [fun test_rate/1]}
	]
    }.
