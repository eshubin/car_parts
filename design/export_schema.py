# Requires pyodbc library (tested on version 3.0.2 win32)

import pyodbc

connection = pyodbc.connect('DRIVER={Transbase ODBC TECDOC CD 4_2011};SERVER=localhost;DATABASE=TECDOC_CD_4_2011;UID=tecdoc;PWD=tcd_error_0')
cursor = connection.cursor()

tables = list()

for table in cursor.tables(table='TOF_%'):
    if not table.table_name.startswith('TOF_GRA_DATA'):
        tables.append(table.table_name)

keys = dict()

for key in cursor.primaryKeys(table='TOF_%'):
    if not key.table_name.startswith('TOF_GRA_DATA'):
        if key.table_name in keys:
            keys[key.table_name] += ',' + key.column_name
        else:
            keys[key.table_name] = key.column_name

for table in tables:
    query = 'create table ' + table + ' ('
    for column in cursor.columns(table=table):
        if column.type_name == 'varchar':
            query += column.column_name + ' varchar(' + str(column.column_size) + ')'
        elif column.type_name == 'bits':
            query += column.column_name + ' binary(' + str(column.column_size / 8) + ')'
        else:
            query += column.column_name + ' ' + column.type_name
        if not column.nullable:
            query += ' NOT NULL'
        query += ','
    query = query.rstrip(',')
    query += ');'
    print query
    print 'alter table ' + table + ' add primary key (' + keys[table] + ');'

