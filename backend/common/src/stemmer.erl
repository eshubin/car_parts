-module(stemmer).

-include_lib("eunit/include/eunit.hrl"). 

-behaviour(gen_server).
-define(SERVER, ?MODULE).

%% ------------------------------------------------------------------
%% API Function Exports
%% ------------------------------------------------------------------

-export([start_link/0, stemm/1]).

%% ------------------------------------------------------------------
%% gen_server Function Exports
%% ------------------------------------------------------------------

-export([init/1, handle_call/3]).

%% ------------------------------------------------------------------
%% API Function Definitions
%% ------------------------------------------------------------------

start_link() ->
    gen_server:start_link({local, ?SERVER}, ?MODULE, [], []).

stemm(Word) ->
    gen_server:call(?SERVER, Word).

%% ------------------------------------------------------------------
%% gen_server Function Definitions
%% ------------------------------------------------------------------

init(_) ->
    {ok, Stemmer} = snowball:new(),
    {ok, Stemmer}.

handle_call(Word, _From, Stemmer) when is_list(Word) ->
    {reply, snowball:stem(Stemmer, Word), Stemmer}.

%% ------------------------------------------------------------------
%% Internal Function Definitions
%% ------------------------------------------------------------------

fixture_test_() ->
    {setup,
     local,
     fun() ->
	     start_link()
     end,
     [
      ?_assertEqual("собак", stemmer:stemm("собака"))      
     ]
    }.
