-module(logic_formula).

-export([new/0, delete/1]).
-export([create_literal/2, do_or/2, do_and/2, do_not/2, match/2]).

-include("logic_formula.hrl").


-include_lib("eunit/include/eunit.hrl").

-spec new() ->
		 formula().
new() ->
    tree:new().

-spec delete(formula()) ->
		 true.
delete(Formula) ->
    tree:delete(Formula).


-spec create_literal(formula(), #literal{}) ->
			    expression().
create_literal(Formula, Literal) when is_record(Literal, literal) ->
    tree:create_node(Formula, Literal).

-spec do_and(formula(), [expression()]) ->
			    expression().

do_and(
    Formula,
    Expressions
   ) when is_list(Expressions)  andalso (length(Expressions) > 1) ->
    binary_operation(Formula, Expressions, 'and').

-spec do_or(formula(), [expression()]) ->
			    expression().

do_or(
    Formula,
    Expressions
   ) when is_list(Expressions) andalso (length(Expressions) > 1) ->
    binary_operation(Formula, Expressions, 'or').

-spec do_not(formula(), expression()) ->
			    expression().
do_not(
    Formula,
    Subformula
   )->
    Parent = tree:create_node(Formula, 'not'),
    tree:add_children(Formula, Parent, [Subformula]),
    Parent.

-spec binary_operation(formula(), [expression()], binary_logic_operation()) ->
			    expression().
binary_operation(
    Formula,
    Expressions,
    Type
   ) when ?IS_BINARY_LOGIC_OPERATOR(Type) andalso 
	  is_list(Expressions) andalso (length(Expressions) > 1)->
    Parent = tree:create_node(Formula, Type),
    lists:foreach(
	fun(Expr) ->
		tree:add_children(
		    Formula,
		    Parent,
		    operation_children(Formula, {Expr, tree:get_data(Formula, Expr)}, Type)
		   )
	end,
	Expressions
       ),
    Parent.

operation_children(Formula, {Expression, Type}, Type) ->
    Children = tree:get_children(Formula, Expression),
    tree:delete_node(Formula, Expression),
    Children;
operation_children(_, {Expression, _}, _) ->
    [Expression].

match(
    Formula,
    Document
) ->
    case tree:get_root(Formula)
    of
	{yes, Root} ->
	    match(Formula, Document, {Root, tree:get_data(Formula, Root)});
	no ->
	    true
    end.


match(
    Formula,
    Document,
    {Expression, 'and'}
   ) ->
    lists:all(
	fun(Child) ->
		match(Formula, Document, {Child, tree:get_data(Formula, Child)})
	end,
	tree:get_children(Formula, Expression)
       );
match(
    Formula,
    Document,
    {Expression, 'or'}
   ) ->
    lists:any(
	fun(Child) ->
		match(Formula, Document, {Child, tree:get_data(Formula, Child)})
	end,
	tree:get_children(Formula, Expression)
       );
match(
    Formula,
    Document,
    {Expression, 'not'}
   ) ->
    [Child] = tree:get_children(Formula, Expression),
    not match(Formula, Document, {Child, tree:get_data(Formula, Child)});
match(
    _Formula,
    Document,
    {_, #literal{property = Prop} = Lit}
   ) ->
    match_leaf(document:get_value(Document, Prop), Lit).



match_leaf(
    {ok, DocValue},
    #literal{condition = in, value = Value}
) ->
    lists:memeber(DocValue, Value);
match_leaf(
    {ok, DocValue},
    #literal{condition = greater_or_equal, value = Value}
) ->
    DocValue >= Value;
match_leaf(
    {ok, DocValue},
    #literal{condition = less_or_equal, value = Value}
) ->
    DocValue =< Value;
match_leaf(
    {ok, DocValue},
    #literal{condition = greater, value = Value}
) ->
    DocValue > Value;
match_leaf(
    {ok, DocValue},
    #literal{condition = less, value = Value}
) ->
    DocValue < Value;
match_leaf(
    {ok, DocValue},
    #literal{condition = equal, value = Value}
) ->
    DocValue =:= Value;
match_leaf(
    error,
    #literal{condition = equal, value = null}
) ->
    true;
match_leaf(
    error,
    _
) ->
    false.


%% ===================================================================
%% EUnit tests
%% ===================================================================


test_matching(Formula)->
    EmptyDoc = document:new(),
    ?assert(match(Formula, EmptyDoc)),
    P1Cond = create_literal(Formula, #literal{property = p1, condition = equal, value = 2}),
    ?assertNot(match(Formula, EmptyDoc)),
    Doc1 = document:set_value(EmptyDoc, p1, 2),
    ?assert(match(Formula, Doc1)),
    P2Cond = create_literal(Formula, #literal{property = p2, condition = less, value = 5}),
    P3Cond = do_and(Formula, [P1Cond, P2Cond]),
    ?assertNot(match(Formula, Doc1)),
    Doc2 = document:set_value(Doc1, p2, 3),
    ?assert(match(Formula, Doc2)),   
    P4Cond = create_literal(Formula, #literal{property = p8, condition = greater, value = 5}),
    do_and(Formula, [P4Cond, P3Cond]),
    ?assertNot(match(Formula, Doc2)),
    Doc3 = document:set_value(Doc2, p8, 8),
    ?assert(match(Formula, Doc3)).

fixture_test_() ->
    {foreach,
     local,
     fun new/0,
     fun delete/1,
     [
      {with, [fun test_matching/1]}
     ]
    }.
