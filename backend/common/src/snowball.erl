-module(snowball).

-export([new/0, stem/2]).

-include_lib("eunit/include/eunit.hrl").

-on_load(on_load/0).

-define(nif_stub, nif_stub_error(?LINE)).
nif_stub_error(Line) ->
    erlang:nif_error({nif_not_loaded,module,?MODULE,line,Line}).


on_load() ->
    PrivDir = case code:priv_dir(?MODULE) of
                  {error, bad_name} ->
                      EbinDir = filename:dirname(code:which(?MODULE)),
                      AppPath = filename:dirname(EbinDir),
                      filename:join(AppPath, "priv");
                  Path ->
                      Path
              end,
    erlang:load_nif(filename:join(PrivDir, ?MODULE), 0).

%% NIFs
new() ->
    ?nif_stub.

stem(_Stemmer, _String) ->
    ?nif_stub.

%% ===================================================================
%% EUnit tests
%% ===================================================================

initialization_test()->
    ?assertMatch({ok, _}, new()).

test_stemming(Stemmer) ->
    Text = "собака",
    ?assertEqual("собак", stem(Stemmer, Text)),
    ?assertEqual("bbb", stem(Stemmer, "bbb")),
    ?assertEqual("aaa", stem(Stemmer, "aaa")).

fixture_test_() ->
    {foreach,
     fun() ->
	     {ok, Stemmer} = new(),
	     Stemmer
     end,
     [
      {with, [fun test_stemming/1]}     
     ]
    }.
