-module(parser_utils).

-export([to_logic_formula/3]).

-include_lib("eunit/include/eunit.hrl").

to_logic_formula(
    Formula,
    {dict_word, WordId},
    WordId2Condition
)->
    logic_formula:add_literal(
	Formula,
	WordId2Condition(WordId)
       );
to_logic_formula(
    Formula,
    {'and', Expressions},
    WordId2Condition
) ->
    logic_formula:do_and(
	Formula,
	[to_logic_formula(Formula, X, WordId2Condition) || X <- Expressions]
       );
to_logic_formula(
    Formula,
    {'or', Expressions},
    WordId2Condition
) ->
    logic_formula:do_or(
	Formula,
	[to_logic_formula(Formula, X, WordId2Condition) || X <- Expressions]
       ).
