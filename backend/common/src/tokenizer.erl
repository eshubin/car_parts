-module(tokenizer).

-export([new/1, get_tokens/2]).

-include_lib("eunit/include/eunit.hrl").

-record(lexer, {
	    re :: re:mp(),
	    dict :: dict()
	   }).

-spec new(dict()) ->
		 #lexer{}.
new(Dictionary)->
    Words = dict:fetch_keys(Dictionary),
    SortedWords = lists:sort(
		      fun(Lhs, Rhs) ->
			      length(Lhs) > length(Rhs)
		      end,
		      Words
		     ),
    {ok, MP} = re:compile(
		   [
		    "(?<=\s|^)(",
		    string:join(SortedWords, "|"),
		    ")(?=\s|$)"
		   ],
		   [unicode]
		  ),
    #lexer{re = MP, dict = Dictionary}.

-spec tokenize(re:mp(), string()) ->
		      [string()].
tokenize(MP, Str) ->
    case re:run(Str, MP, [global, {capture, all_but_first, list}])
    of
	{match, MatchingList} ->
	    [X || [X] <- MatchingList];
	nomatch ->
	    []
    end.

-spec get_tokens(#lexer{}, string()) ->
			[{dict_word, [integer()]}].
get_tokens(#lexer{re = Re, dict = D}, Str) ->
    [{dict_word, dict:fetch(X, D)} || X <- tokenize(Re, Str)].

test_tokens(Lexer) ->
    ?assertEqual(get_tokens(Lexer, "aaa ccc ccc"), [{dict_word, [45]}, {dict_word, [4]}]).

test_matching(#lexer{re =Tokenizer}) ->
    ?assertEqual(tokenize(Tokenizer, "aaa"), ["aaa"]),
    ?assertEqual(tokenize(Tokenizer, "aaa bbb"), ["aaa", "bbb"]),
    ?assertEqual(tokenize(Tokenizer, "aaa ccc ccc"), ["aaa ccc", "ccc"]),
    ?assertEqual(tokenize(Tokenizer, "aaaccc"), []),
    ?assertEqual(tokenize(Tokenizer, "абвг"), ["абвг"]),
    ?assertEqual(tokenize(Tokenizer, "not_exist_in_dictionary"), []).

fixture_test_() ->
    {foreach,
     fun() ->
	     D = dict:from_list([
				 {"aaa", [1,2]},
				 {"bbb", [3]},
				 {"ccc", [4]},
				 {"aaa ccc", [45]},
				 {"абвг", [32]}
				]),
	     new(D)
     end,
     [
      {with, [fun test_matching/1]},
      {with, [fun test_tokens/1]}
     ]
    }.

