-module(parser_test).

-include_lib("eunit/include/eunit.hrl").

matching_test_() ->
    [
     ?_assertMatch(
	  {ok, _},
	  parser:parse([{dict_word, [45]}, {dict_word, [4]}])
	 ),
     ?_assertMatch(
	  {error, _},
	  parser:parse([{dict_word, [45]}, {unknown, 1},  {dict_word, [4]}])
	 )
    ].

value_test_() ->
    [
     ?_assertEqual(
	  {ok, null},
	  parser:parse([])
	 ),
     ?_assertEqual(
	  {ok, {dict_word, 4}},
	  parser:parse([{dict_word, [4]}])
	 ),
     ?_assertEqual(
	  {ok, {'or', [{dict_word, 4}, {dict_word, 9}]}},
	  parser:parse([{dict_word, [4,9]}])
	 ),
     ?_assertEqual(
     	  {ok, {'and', [{dict_word, 45}, {dict_word, 49}]}},
     	  parser:parse([{dict_word, [45]}, {dict_word, [49]}])
     	 )
    ].
    

test_with_tokenizer(Tokenizer) ->
     ?assertMatch(
	  {ok, _},
	  parser:parse(tokenizer:get_tokens(Tokenizer, ""))
	 ),    
     ?assertMatch(
	  {ok, _},
	  parser:parse(tokenizer:get_tokens(Tokenizer, "aaa ccc bbb"))
	 ).    

fixture_test_() ->
    {foreach,
     fun() ->
	     D = dict:from_list([
				 {"aaa", [1,2]},
				 {"bbb", [3]},
				 {"ccc", [4]},
				 {"aaa ccc", [45]},
				 {"абвг", [32]}
				]),
	     tokenizer:new(D)
     end,
     [
      {with, [fun test_with_tokenizer/1]}
     ]
    }.

