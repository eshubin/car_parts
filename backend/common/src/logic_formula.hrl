-ifndef(logic_formula_hrl).
-define(logic_formula_hrl, 1).


-type formula() :: tree:tree().
-type expression() :: tree:tree_node().
-type binary_logic_operation() :: 'and' | 'or'.

-type property() :: term().
-type value() :: term().


-define(IS_BINARY_LOGIC_OPERATOR(Op),
	(
	    (Op =:= 'and') orelse
	    (Op =:= 'or')
	)).

-define(IS_CONDITION(Op),
	(
	    (Op =:= 'in') orelse
	    (Op =:= 'less') orelse
	    (Op =:= 'equal') orelse
	    (Op =:= 'less_or_equal') orelse
	    (Op =:= 'greater_or_equal') orelse
	    (Op =:= 'greater')
	)).

-record(literal,{
	property :: property(),
	condition :: in | less | equal | less_or_equal | greater_or_equal | greater,
	value :: value()}).

-endif. %logic_formula_hrl
