#include "erl_nif.h"

#include <libstemmer.h>
#include <syslog.h>
#include <string.h>


static ErlNifResourceType* snowball_RESOURCE = NULL;

typedef struct
{
  struct sb_stemmer *stemmer_;
} snowball_handle;

static void snowball_resource_cleanup(ErlNifEnv* env, void* arg)
{
  snowball_handle* handle = (snowball_handle*) arg;
  sb_stemmer_delete(handle->stemmer_);
}

static int load(ErlNifEnv* env, void** priv_data, ERL_NIF_TERM load_info)
{
    ErlNifResourceFlags flags = ERL_NIF_RT_CREATE  | ERL_NIF_RT_TAKEOVER;
    ErlNifResourceType* rt = enif_open_resource_type(env, NULL,
                                                     "snowball",
                                                     &snowball_resource_cleanup,
                                                     flags, NULL);
    if (rt == NULL)
        return -1;

    snowball_RESOURCE = rt;
    return 0;
}


static ERL_NIF_TERM snowball_new(ErlNifEnv* env, int argc, const ERL_NIF_TERM argv[])
{
    snowball_handle* handle = enif_alloc_resource(
    						  snowball_RESOURCE,
    						  sizeof(snowball_handle)
    						  );
    handle->stemmer_ = sb_stemmer_new("russian", NULL);

    ERL_NIF_TERM ret;
    if (handle->stemmer_)
      {
    	ERL_NIF_TERM result = enif_make_resource(env, handle);

    	ret = enif_make_tuple2(env, enif_make_atom(env, "ok"), result);
      }
    else
      {
    	ret = enif_make_atom(env, "error");
      }
    enif_release_resource(handle);
    return ret;
}


static ERL_NIF_TERM snowball_stem(ErlNifEnv* env, int argc, const ERL_NIF_TERM argv[])
{
  snowball_handle *handle;
  char input_string[256];
  if (!enif_get_resource(env, argv[0], snowball_RESOURCE, &handle))
    {
      return enif_make_badarg(env);
    }
  int length = enif_get_string(env, argv[1], input_string, sizeof(input_string), ERL_NIF_LATIN1);
  if (!length)
    {
      return enif_make_badarg(env);
    }

  /* syslog(LOG_DEBUG | LOG_USER, "%s - %d", input_string, length); */

  const sb_symbol * res = sb_stemmer_stem(
					  handle->stemmer_,
					  input_string,
					  strnlen(input_string, sizeof(input_string))
					  );
  /* syslog(LOG_DEBUG | LOG_USER, "%s - %d", res, sb_stemmer_length(handle->stemmer_)); */
  return enif_make_string(env, res, ERL_NIF_LATIN1);
}

static ErlNifFunc nif_funcs[] =
{
  {"new", 0, snowball_new},
  {"stem", 2, snowball_stem}
};

ERL_NIF_INIT(snowball,nif_funcs,load,NULL,NULL,NULL);
