import pyodbc

def exportBrands(source, destination):
    outCursor = destination.cursor()
    outCursor.execute(
        'DROP TABLE IF EXISTS brands;')
    outCursor.execute(
        'CREATE TABLE brands (' +
            'id INT NOT NULL PRIMARY KEY, ' +
            'code VARCHAR(256) NOT NULL, ' +
            'name VARCHAR(256) NOT NULL, ' +
            'num INT NOT NULL UNIQUE KEY);')
    inCursor = source.cursor()
    for row in inCursor.execute(
        'SELECT ' +
            'BRA_ID, ' +
            'BRA_MFC_CODE, ' +
            'BRA_BRAND, ' +
            'BRA_MF_NR ' +
        'FROM TECDOC.TOF_BRANDS'):
        outCursor.execute(
            'INSERT INTO brands ' +
            'VALUES (?, ?, ?, ?);',
            row.BRA_ID,
            row.BRA_MFC_CODE,
            row.BRA_BRAND,
            row.BRA_MF_NR)

def exportManufacturers(source, destination):
    outCursor = destination.cursor()
    outCursor.execute(
        'DROP TABLE IF EXISTS manufacturers;')
    outCursor.execute(
        'CREATE TABLE manufacturers (' +
            'id INT NOT NULL PRIMARY KEY, ' +
            'makePersonal BIT NOT NULL, ' +
            'makeCommercail BIT NOT NULL, ' +
            'makeAxis BIT NOT NULL, ' +
            'makeEngine BIT NOT NULL, ' +
            'engineType INT NOT NULL, ' +
            'brandCode VARCHAR(256) NOT NULL, ' +
            'brandName VARCHAR(256) NOT NULL, ' +
            'brandNum INT NOT NULL, ' +
            'FOREIGN KEY (brandNum) REFERENCES brands(num));')
    inCursor = source.cursor()
    for row in inCursor.execute(
        'SELECT ' +
            'MFA_ID, ' +
            'MFA_PC_MFC, ' +
            'MFA_CV_MFC, ' +
            'MFA_AXL_MFC, ' +
            'MFA_ENG_MFC, ' +
            'MFA_ENG_TYP, ' +
            'MFA_MFC_CODE, ' +
            'MFA_BRAND, ' +
            'MFA_MF_NR ' +
        'FROM TECDOC.TOF_MANUFACTURERS'):
        outCursor.execute(
            'INSERT INTO manufacturers ' +
            'VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?);',
            row.MFA_ID,
            row.MFA_PC_MFC,
            row.MFA_CV_MFC,
            row.MFA_AXL_MFC,
            row.MFA_ENG_MFC,
            row.MFA_ENG_TYP,
            row.MFA_MFC_CODE,
            row.MFA_BRAND,
            row.MFA_MF_NR)

def exportModels(source, destination):
    outCursor = destination.cursor()
    outCursor.execute(
        'DROP TABLE IF EXISTS models;')
    outCursor.execute(
        'CREATE TABLE models (' +
            'id INT NOT NULL PRIMARY KEY, ' +
            'manufacturerId INT NOT NULL, ' +
            'name VARCHAR(256) NOT NULL, ' +
            'start DATE NULL, ' +
            'end DATE NULL, ' +
            'isPersonal BIT NOT NULL, ' +
            'isCommercail BIT NOT NULL, ' +
            'isAxis BIT NOT NULL, ' +
            'FOREIGN KEY (manufacturerId) REFERENCES manufacturers(id));')
    inCursor = source.cursor()
    for row in inCursor.execute(
        'SELECT ' +
            'MOD_ID, ' +
            'MOD_MFA_ID, ' +
            'TEX_TEXT, ' +
            'MOD_PCON_START, ' +
            'MOD_PCON_END, ' +
            'MOD_PC, ' +
            'MOD_CV, ' +
            'MOD_AXL ' +
        'FROM TECDOC.TOF_MODELS ' +
        'JOIN TECDOC.TOF_COUNTRY_DESIGNATIONS ' +
            'ON CDS_ID = MOD_CDS_ID AND CDS_LNG_ID = 16 AND CDS_CTM subrange(144 cast integer) = 1 ' +
        'JOIN TECDOC.TOF_DES_TEXTS ON TEX_ID = CDS_TEX_ID'):
        outCursor.execute(
            'INSERT INTO models ' +
            'VALUES (?, ?, ?, STR_TO_DATE(?, \'%Y%m\'), STR_TO_DATE(?, \'%Y%m\'), ?, ?, ?);',
            row.MOD_ID,
            row.MOD_MFA_ID,
            unicode(row.TEX_TEXT, 'cp1251'),
            row.MOD_PCON_START,
            row.MOD_PCON_END,
            row.MOD_PC,
            row.MOD_CV,
            row.MOD_AXL)

def exportTypes(source, destination):
    # todo more columns
    outCursor = destination.cursor()
    outCursor.execute(
        'DROP TABLE IF EXISTS types;')
    outCursor.execute(
        'CREATE TABLE types (' +
            'id INT NOT NULL PRIMARY KEY, ' +
            'name VARCHAR(256) NOT NULL, ' +
            'fullName VARCHAR(256) NOT NULL, ' +
            'modelId INT NOT NULL, ' +
            'sort INT NOT NULL, ' +
            'start DATE NULL, ' +
            'end DATE NULL, ' +
            'kw INT NULL, ' +
            'hp INT NULL, ' +
            'capacity INT NULL, ' +
            'cylinders INT NULL, ' +
            'doors INT NULL, ' +
            'tank INT NULL, ' +
            'valves INT NULL, ' +
            'FOREIGN KEY (modelId) REFERENCES models(id));')
    inCursor = source.cursor()
    for row in inCursor.execute(
        'SELECT ' +
            'TYP_ID, ' +
            'T1.TEX_TEXT AS NAME, ' +
            'T2.TEX_TEXT AS FULL_NAME, ' +
            'TYP_MOD_ID, ' +
            'TYP_SORT, ' +
            'TYP_PCON_START, ' +
            'TYP_PCON_END, ' +
            'TYP_KW_FROM, ' +
            'TYP_HP_FROM, ' +
            'TYP_CCM, ' +
            'TYP_CYLINDERS, ' +
            'TYP_DOORS, ' +
            'TYP_TANK, ' +
            'TYP_VALVES ' +
        'FROM TECDOC.TOF_TYPES ' +
        'JOIN TECDOC.TOF_COUNTRY_DESIGNATIONS D1 ' +
            'ON D1.CDS_ID = TYP_CDS_ID AND D1.CDS_LNG_ID = 16 AND D1.CDS_CTM subrange(144 cast integer) = 1 ' +
        'JOIN TECDOC.TOF_DES_TEXTS T1 ON T1.TEX_ID = D1.CDS_TEX_ID ' +
        'JOIN TECDOC.TOF_COUNTRY_DESIGNATIONS D2 ' +
            'ON D2.CDS_ID = TYP_MMT_CDS_ID AND D2.CDS_LNG_ID = 16 AND D2.CDS_CTM subrange(144 cast integer) = 1 ' +
        'JOIN TECDOC.TOF_DES_TEXTS T2 ON T2.TEX_ID = D2.CDS_TEX_ID'):
        outCursor.execute(
            'INSERT INTO types ' +
            'VALUES (?, ?, ?, ?, ?, STR_TO_DATE(?, \'%Y%m\'), STR_TO_DATE(?, \'%Y%m\'), ?, ?, ?, ?, ?, ?, ?);',
            row.TYP_ID,
            unicode(row.NAME, 'cp1251'),
            unicode(row.FULL_NAME, 'cp1251'),
            row.TYP_MOD_ID,
            row.TYP_SORT,
            row.TYP_PCON_START,
            row.TYP_PCON_END,
            row.TYP_KW_FROM,
            row.TYP_HP_FROM,
            row.TYP_CCM,
            row.TYP_CYLINDERS,
            row.TYP_DOORS,
            row.TYP_TANK,
            row.TYP_VALVES)

def exportSuppliers(source, destination):
    outCursor = destination.cursor()
    outCursor.execute(
        'DROP TABLE IF EXISTS suppliers;')
    outCursor.execute(
        'CREATE TABLE suppliers (' +
            'id INT NOT NULL PRIMARY KEY, ' +
            'name VARCHAR(256) NOT NULL, ' +
            'num INT NOT NULL UNIQUE KEY);')
    inCursor = source.cursor()
    for row in inCursor.execute(
        'SELECT ' +
            'SUP_ID, ' +
            'SUP_BRAND, ' +
            'SUP_SUPPLIER_NR ' +
        'FROM TECDOC.TOF_SUPPLIERS'):
        outCursor.execute(
            'INSERT INTO suppliers ' +
            'VALUES (?, ?, ?);',
            row.SUP_ID,
            unicode(row.SUP_BRAND, 'cp1251'),
            row.SUP_SUPPLIER_NR)

def exportArticles(source, destination):
    outCursor = destination.cursor()
    outCursor.execute(
        'DROP TABLE IF EXISTS articles;')
    outCursor.execute(
        'CREATE TABLE articles (' +
            'id INT NOT NULL PRIMARY KEY, ' +
            'num VARCHAR(256) NOT NULL, ' +
            'supplierId INT NOT NULL, ' +
            'isSelf BIT NULL, ' +
            'isMark BIT NULL, ' +
            'isReplacement BIT NULL, ' +
            'isAccessory BIT NULL, ' +
            'FOREIGN KEY (supplierId) REFERENCES suppliers(id));')
    inCursor = source.cursor()
    for row in inCursor.execute(
        'SELECT ' +
            'ART_ID, ' +
            'ART_ARTICLE_NR, ' +
            'ART_SUP_ID, ' +
            'ART_PACK_SELFSERVICE, ' +
            'ART_MATERIAL_MARK, ' +
            'ART_REPLACEMENT, ' +
            'ART_ACCESSORY ' +
        'FROM TECDOC.TOF_ARTICLES'):
        outCursor.execute(
            'INSERT INTO articles ' +
            'VALUES (?, ?, ?, ?, ?, ?, ?);',
            row.ART_ID,
            row.ART_ARTICLE_NR,
            row.ART_SUP_ID,
            row.ART_PACK_SELFSERVICE,
            row.ART_MATERIAL_MARK,
            row.ART_REPLACEMENT,
            row.ART_ACCESSORY)

def exportGenericArticles(source, destination):
    outCursor = destination.cursor()
    outCursor.execute(
        'DROP TABLE IF EXISTS genericArticles;')
    outCursor.execute(
        'CREATE TABLE genericArticles (' +
            'id INT NOT NULL PRIMARY KEY, ' +
            'num INT NOT NULL, ' +
            'name VARCHAR(256) NOT NULL, ' +
            'standard VARCHAR(256) NOT NULL, ' +
            'assembly VARCHAR(256) NULL, ' +
            'intended VARCHAR(256) NULL);')
    inCursor = source.cursor()
    for row in inCursor.execute(
        'SELECT ' +
            'GA_ID, ' +
            'GA_NR, ' +
            'T1.TEX_TEXT AS NAME, ' +
            'T2.TEX_TEXT AS STANDARD, ' +
            'T3.TEX_TEXT AS ASSEMBLY, ' +
            'T4.TEX_TEXT AS INTENDED ' +
        'FROM TECDOC.TOF_GENERIC_ARTICLES ' +
        'JOIN TECDOC.TOF_DESIGNATIONS D1 ON D1.DES_ID = GA_DES_ID AND D1.DES_LNG_ID = 16 ' +
        'JOIN TECDOC.TOF_DES_TEXTS T1 ON T1.TEX_ID = D1.DES_TEX_ID ' +
        'JOIN TECDOC.TOF_DESIGNATIONS D2 ON D2.DES_ID = GA_DES_ID_STANDARD AND D2.DES_LNG_ID = 16 ' +
        'JOIN TECDOC.TOF_DES_TEXTS T2 ON T2.TEX_ID = D2.DES_TEX_ID ' +
        'LEFT JOIN TECDOC.TOF_DESIGNATIONS D3 ON D3.DES_ID = GA_DES_ID_ASSEMBLY AND D3.DES_LNG_ID = 16 ' +
        'LEFT JOIN TECDOC.TOF_DES_TEXTS T3 ON T3.TEX_ID = D3.DES_TEX_ID ' +
        'LEFT JOIN TECDOC.TOF_DESIGNATIONS D4 ON D4.DES_ID = GA_DES_ID_INTENDED AND D4.DES_LNG_ID = 16 ' +
        'LEFT JOIN TECDOC.TOF_DES_TEXTS T4 ON T4.TEX_ID = D4.DES_TEX_ID'):
        outCursor.execute(
            'INSERT INTO genericArticles ' +
            'VALUES (?, ?, ?, ?, ?, ?);',
            row.GA_ID,
            row.GA_NR,
            unicode(row.NAME, 'cp1251'),
            unicode(row.STANDARD, 'cp1251'),
            None if row.ASSEMBLY is None else unicode(row.ASSEMBLY, 'cp1251'),
            None if row.INTENDED is None else unicode(row.INTENDED, 'cp1251'))

def exportTree(source, destination):
    outCursor = destination.cursor()
    outCursor.execute(
        'DROP TABLE IF EXISTS tree;')
    outCursor.execute(
        'CREATE TABLE tree (' +
            'id INT NOT NULL PRIMARY KEY, ' +
            'parentId INT NULL, ' +
            'type INT NOT NULL, ' +
            'level INT NOT NULL, ' +
            'name VARCHAR(256) NOT NULL, ' +
            'sort INT NOT NULL, ' +
            'num INT NOT NULL);')
    inCursor = source.cursor()
    for row in inCursor.execute(
        'SELECT ' +
            'STR_ID, ' +
            'STR_ID_PARENT, ' +
            'STR_TYPE, ' +
            'STR_LEVEL, ' +
            'TEX_TEXT, ' +
            'STR_SORT, ' +
            'STR_NODE_NR ' +
        'FROM TECDOC.TOF_SEARCH_TREE ' +
        'JOIN TECDOC.TOF_DESIGNATIONS ON DES_ID = STR_DES_ID AND DES_LNG_ID = 16 ' +
        'JOIN TECDOC.TOF_DES_TEXTS ON TEX_ID = DES_TEX_ID'):
        outCursor.execute(
            'INSERT INTO tree ' +
            'VALUES (?, ?, ?, ?, ?, ?, ?);',
            row.STR_ID,
            row.STR_ID_PARENT,
            row.STR_TYPE,
            row.STR_LEVEL,
            unicode(row.TEX_TEXT, 'cp1251'),
            row.STR_SORT,
            row.STR_NODE_NR)
    outCursor.execute(
        'ALTER TABLE tree ' +
        'ADD FOREIGN KEY (parentId) REFERENCES tree(id);');

def exportLinkGenericArticlesTree(source, destination):
    outCursor = destination.cursor()
    outCursor.execute(
        'DROP TABLE IF EXISTS linkGenericArticlesTree;')
    outCursor.execute(
        'CREATE TABLE linkGenericArticlesTree (' +
            'nodeId INT NOT NULL, ' +
            'genericArticleId INT NOT NULL);')
    inCursor = source.cursor()
    for row in inCursor.execute(
        'SELECT ' +
            'LGS_STR_ID, ' +
            'LGS_GA_ID ' +
        'FROM TECDOC.TOF_LINK_GA_STR'):
        outCursor.execute(
            'INSERT INTO linkGenericArticlesTree ' +
            'VALUES (?, ?);',
            row.LGS_STR_ID,
            row.LGS_GA_ID)
    outCursor.execute(
        'ALTER TABLE linkGenericArticlesTree ' +
        'ADD FOREIGN KEY (nodeId) REFERENCES tree(id);');
    outCursor.execute(
        'ALTER TABLE linkGenericArticlesTree ' +
        'ADD FOREIGN KEY (genericArticleId) REFERENCES genericArticles(id);');

def exportLinkArticlesGenericArticles(source, destination):
    outCursor = destination.cursor()
    outCursor.execute(
        'DROP TABLE IF EXISTS linkArticlesGenericArticles;')
    outCursor.execute(
        'CREATE TABLE linkArticlesGenericArticles (' +
            'articleId INT NOT NULL, ' +
            'genericArticleId INT NOT NULL, ' +
            'supplierId INT NOT NULL);')
    inCursor = source.cursor()
    for row in inCursor.execute(
        'SELECT ' +
            'LAG_ART_ID, ' +
            'LAG_GA_ID, ' +
            'LAG_SUP_ID ' +
        'FROM TECDOC.TOF_LINK_ART_GA'):
        outCursor.execute(
            'INSERT INTO linkArticlesGenericArticles ' +
            'VALUES (?, ?, ?);',
            row.LAG_ART_ID,
            row.LAG_GA_ID,
            row.LAG_SUP_ID)
    outCursor.execute(
        'DELETE FROM linkArticlesGenericArticles ' +
        'USING linkArticlesGenericArticles ' +
        'LEFT JOIN articles on articles.id = linkArticlesGenericArticles.articleId ' +
        'WHERE articles.id IS NULL;');
    outCursor.execute(
        'ALTER TABLE linkArticlesGenericArticles ' +
        'ADD FOREIGN KEY (articleId) REFERENCES articles(id), ' +
        'ADD FOREIGN KEY (genericArticleId) REFERENCES genericArticles(id), ' +
        'ADD FOREIGN KEY (supplierId) REFERENCES suppliers(id);');

def exportLinkArticles(source, destination):
    outCursor = destination.cursor()
    outCursor.execute(
        'DROP TABLE IF EXISTS linkArticles;')
    outCursor.execute(
        'CREATE TABLE linkArticles (' +
            'id INT NOT NULL PRIMARY KEY, ' +
            'articleId INT NOT NULL, ' +
            'genericArticleId INT NOT NULL, ' +
            'sort INT NOT NULL, ' +
            'FOREIGN KEY (articleId) REFERENCES articles(id), ' +
            'FOREIGN KEY (genericArticleId) REFERENCES genericArticles(id));')
    inCursor = source.cursor()
    for row in inCursor.execute(
        'SELECT ' +
            'LA_ID, ' +
            'LA_ART_ID, ' +
            'LA_GA_ID, ' +
            'LA_SORT ' +
        'FROM TECDOC.TOF_LINK_ART'):
        outCursor.execute(
            'INSERT INTO linkArticles ' +
            'VALUES (?, ?, ?, ?);',
            row.LA_ID,
            row.LA_ART_ID,
            row.LA_GA_ID,
            row.LA_SORT)

def exportLinkLinkArticlesTypes(source, destination):
    outCursor = destination.cursor()
    outCursor.execute(
        'DROP TABLE IF EXISTS linkLinkArticlesTypes;')
    outCursor.execute(
        'CREATE TABLE linkLinkArticlesTypes (' +
            'typeId INT NOT NULL, ' +
            'linkArticleId INT NOT NULL, ' +
            'genericArticleId INT NOT NULL, ' +
            'supplierId INT NOT NULL, ' +
            'sort INT NOT NULL);')
            #'sort INT NOT NULL, ' +
            #'FOREIGN KEY (typeId) REFERENCES types(id), ' +
            #'FOREIGN KEY (linkArticleId) REFERENCES linkArticles(id), ' +
            #'FOREIGN KEY (genericArticleId) REFERENCES genericArticles(id), ' +
            #'FOREIGN KEY (supplierId) REFERENCES suppliers(id));')
            # SET foreign_key_checks = 0/1;
    inCursor = source.cursor()
    i = 1
    for row in inCursor.execute(
        'SELECT ' +
            'LAT_TYP_ID, ' +
            'LAT_LA_ID, ' +
            'LAT_GA_ID, ' +
            'LAT_SUP_ID, ' +
            'LAT_SORT ' +
        'FROM TECDOC.TOF_LINK_LA_TYP'):
        outCursor.execute(
            'INSERT INTO linkLinkArticlesTypes ' +
            'VALUES (?, ?, ?, ?, ?);',
            row.LAT_TYP_ID,
            row.LAT_LA_ID,
            row.LAT_GA_ID,
            row.LAT_SUP_ID,
            row.LAT_SORT)
        i += 1
        if i % 1000000 == 0:
            print(str(i))

source = pyodbc.connect('DRIVER={Transbase ODBC TECDOC CD 4_2011};SERVER=localhost;DATABASE=TECDOC_CD_4_2011;UID=tecdoc;PWD=tcd_error_0')
destination = pyodbc.connect('DRIVER={MySQL ODBC 5.1 Driver};SERVER=localhost;DATABASE=tecdoc;UID=root;PWD=root')

# TODO
# TECDOC.TOF_LINK_LA_TYP - 80M

#exportBrands(source, destination)
#exportManufacturers(source, destination)
#exportModels(source, destination)
#exportTypes(source, destination)
#exportSuppliers(source, destination)
#exportArticles(source, destination)
#exportGenericArticles(source, destination)
#exportTree(source, destination)
#exportLinkGenericArticlesTree(source, destination)
#exportLinkArticlesGenericArticles(source, destination)
#exportLinkArticles(source, destination)
exportLinkLinkArticlesTypes(source, destination)
print('done')

destination.commit()
source.commit()
